package service;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import proto.PersonalData;
import proto.PersonalDataServiceGrpc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PersonalDataClass extends PersonalDataServiceGrpc.PersonalDataServiceImplBase {

    List<PersonalData.Person> persons = new ArrayList<>();

    @Override
    public void getPerson(PersonalData.PersonRequest request, StreamObserver<PersonalData.PersonResponse> responseObserver) {

        PersonalData.PersonResponse.Builder response = PersonalData.PersonResponse.newBuilder();
        PersonalData.Person.Builder person = null;

        PersonalData.Person search = persons.stream().filter(element -> element.getName().equals(request.getName())).findFirst().orElse(null);

        if(search == null) {
            Status status = Status.NOT_FOUND.withDescription("Person with this name not found");
            responseObserver.onError(status.asRuntimeException());
        }else
            person = search.toBuilder();
        response.setPerson(person);

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getNames(PersonalData.Empty request, StreamObserver<PersonalData.Person> responseObserver) {

        if(this.persons.size() != 0) {
            for(PersonalData.Person person : this.persons) {
                responseObserver.onNext(person);
            }
            responseObserver.onCompleted();
        }else {
            Status status = Status.NOT_FOUND.withDescription("No persons");
            responseObserver.onError(status.asRuntimeException());
        }
    }

    @Override
    public void setPerson(PersonalData.Person request, StreamObserver<PersonalData.Empty> responseObserver) {

        System.out.println("Set person");
        persons.add(request);

        System.out.println(request);
        System.out.println("all array" + this.persons);

        PersonalData.Empty.Builder response = PersonalData.Empty.newBuilder();
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }

    @Override
    public void getAge(PersonalData.PersonRequest request, StreamObserver<PersonalData.AgeResponse> responseObserver) {

        String birthYear = "";
        String cnp = request.getCnp();
        if(cnp.charAt(0) == 1 || cnp.charAt(0) == 2)
        {
            birthYear = "1" + "9" +  cnp.charAt(1) + cnp.charAt(2);
        }else
            if(cnp.charAt(0) == 5 || cnp.charAt(0) == 6)
            {
                birthYear = "2" + "0" +  cnp.charAt(1) + cnp.charAt(2);
            }

        int age = Integer.parseInt(birthYear) - 2021;
    }

    @Override
    public void getGender(PersonalData.PersonRequest request, StreamObserver<PersonalData.GenderResponse> responseObserver) {

        String gender = "";
        String cnp = request.getCnp();

        if(cnp.charAt(0) == 1 || cnp.charAt(0) == 5)
        {
            gender = "masculin";
        }else
            if(cnp.charAt(0) == 2 || cnp.charAt(0) == 6)
            {
                gender = "feminin";
            }

    }
}
