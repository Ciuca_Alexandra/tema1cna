import io.grpc.Server;
import io.grpc.ServerBuilder;
import service.PersonalDataClass;

import java.io.IOException;

public class MainServer {

    public static void main(String[] arg){
        try{
            Server server = ServerBuilder.forPort(8999).addService(new PersonalDataClass()).build();

            server.start();

            System.out.println("Server started at " + server.getPort());

            server.awaitTermination();
        }catch (IOException e) {
            System.out.println("Error: " + e);
        }catch (InterruptedException e) {
            System.out.println("Error: " + e);
        }
    }
}
