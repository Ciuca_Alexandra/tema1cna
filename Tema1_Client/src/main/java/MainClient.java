import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.PersonalData;
import proto.PersonalDataServiceGrpc;

import java.util.Scanner;

public class MainClient {

    public static void main(String[] args){

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();

        PersonalDataServiceGrpc.PersonalDataServiceStub dataStub = PersonalDataServiceGrpc.newStub(channel);

        System.out.println("Menu");
        System.out.println("1. Introduce personal data");
        System.out.println("2. Get everyone's personal data");
        System.out.println("3. Get someone's personal data");
        System.out.println("4. Exit");

        boolean isConnected = true;

        while(isConnected)
        {
            Scanner input = new Scanner(System.in);
            System.out.println("Choose: ");
            int option = input.nextInt();

            switch (option){

                case 1: {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Full name: ");
                    String name = read.nextLine();

                    System.out.println("CNP: ");
                    String cnp = read.next();

                    dataStub.setPerson(
                            PersonalData.Person.newBuilder()
                                    .setName(name).setCnp(cnp).build(), new StreamObserver<PersonalData.Empty>() {
                                @Override
                                public void onNext(PersonalData.Empty empty) {

                                }

                                @Override
                                public void onError(Throwable throwable) {
                                    System.out.println("Error : " + throwable.getMessage());
                                }

                                @Override
                                public void onCompleted() {

                                }
                            });
                    break;
                }

                case 2:
                {
                    dataStub.getNames(PersonalData.Empty.newBuilder().build(), new StreamObserver<PersonalData.Person>() {
                        @Override
                        public void onNext(PersonalData.Person person) {
                            System.out.println("Person " + person.getName() + ", " + person.getCnp());
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            System.out.println("Error : " + throwable.getMessage());
                        }

                        @Override
                        public void onCompleted() {

                        }
                    });
                    break;
                }

                case 3:
                {
                    Scanner read = new Scanner(System.in).useDelimiter("\n");
                    System.out.println("Person's name: ");

                    String name = read.next();

                    dataStub.getPerson(PersonalData.PersonRequest.newBuilder().setName(name).build(), new StreamObserver<PersonalData.PersonResponse>() {
                        @Override
                        public void onNext(PersonalData.PersonResponse personResponse) {
                            System.out.println(personResponse);
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            System.out.println("Error : " + throwable.getMessage());
                        }

                        @Override
                        public void onCompleted() {

                        }
                    });
                    break;
                }

                case 4:
                {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Unknown command, insert a valid command!");
                    break;
            }
        }
    }
}
